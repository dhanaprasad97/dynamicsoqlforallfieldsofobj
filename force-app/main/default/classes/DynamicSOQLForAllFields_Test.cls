/*
 * Created by : Dhana Prasad
 * test class for DynamicSOQLForAllFields class
 */
@isTest
public with sharing class DynamicSOQLForAllFields_Test {
    @isTest
    public static void getMOBILEvarioRoleListTest() { 
        List<MOBILEvario_Role__c> role =TestDataFactory.createMOBILEvarioRoleRecords('test',1,true);
        List<MOBILEvario_Role__c> roleList;
        System.runAs(u) {
            roleList = role[0].Id.getMOBILEvarioRoleList(role[0].Id);
        }
        List<MOBILEvario_Role__c> roleList1 = role[0].Id.getMOBILEvarioRoleList(role[0].Id);
        System.assertEquals(roleList[0].Id, role[0].Id); 
    }
}
