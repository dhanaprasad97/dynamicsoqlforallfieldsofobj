//Created by Dhana Prasad
//Class that helps in querying records of an object with all fields dynamically
public with sharing class DynamicSOQLForAllFields {
    @AuraEnabled
    public static List<MOBILEvario_Role__c> getMOBILEvarioRoleList( String mobileVarioRoleId) {
        String mobileVarioRoleId = null;
            String query = 'SELECT ';
            Set<String> fields = new Set<String>(MOBILEvario_Role__c.sObjectType.getDescribe().fields.getMap().keySet());
            for(String field : fields) {
                query += field+', ';
            }

            query =  query.substring(0, query.length()-2);
            query += ' FROM MOBILEvario_Role__c WHERE Id =\''+mobileVarioRoleId+'\'';
            
            System.debug('Query'+query);
            List<MOBILEvario_Role__c> sobjList = Database.query(query);
            System.debug('sobjList'+sobjList);

            return sobjList;
        
        }
}
