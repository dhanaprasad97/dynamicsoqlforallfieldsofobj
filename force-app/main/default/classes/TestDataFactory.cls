/*
 * Created by : Dhana Prasad
 * class to create test data
 */
public with sharing class TestDataFactory {

    //test method to create mobilevario role records
    public static List<MOBILEvario_Role__c> createMOBILEvarioRoleRecords(String Name,Integer count,Boolean isInsert){
        List<MOBILEvario_Role__c> mobileVarioRoleList = new List<MOBILEvario_Role__c>();
        for(Integer i=0;i<count;i++){
            MOBILEvario_Role__c role = new MOBILEvario_Role__c();
            role.Name=Name+i;
            role.Transit_Account_Block__c = 'yes';
            mobileVarioRoleList.add(role);
        }
        if(isInsert){
            insert mobileVarioRoleList;
        }
        return mobileVarioRoleList;
    }
}
